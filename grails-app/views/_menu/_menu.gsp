<%@ page import="com.seguridad.Usuario" %>
<g:set var="usuario" value="${Usuario.findByUsername(sec.loggedInUserInfo(field:'username'))}"/>

<div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">DHTMLX táctil Visual Designer</a>
            <ul class="dropdown-menu">
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#"><g:message code="menu.infraestructura.label" default="Infraestructura"/></a>
                </li>
                <li class="divider"></li>
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#"><g:message code="menu.tgs.label" default="TGS"/></a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">BÉHAT</a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">IX MARCO</a>
            <ul class="dropdown-menu">
                <li><a href="#">Enalce 1</a></li>
                <li><a href="#">Enalce 2</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">INITIALIZR</a>
            <ul class="dropdown-menu">
                <li><a href="#">Enalce 1</a></li>
                <li><a href="#">Enalce 2</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><g:message code="menu.seguridad.label" default="Seguridad"/> </a>
            <ul class="dropdown-menu">
                <li><g:link controller="user" action="search" ><g:message code="user.label" default="Usuarios"/> </g:link></li>
                <li><g:link controller="role" action="search" ><g:message code="role.label" default="Roles"/> </g:link></li>
                <li><g:link controller="requestmap" action="search" ><g:message code="requestmap.label" default="URL's"/> </g:link></li>
            </ul>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <sec:ifNotLoggedIn>
            <li>
                <g:link controller='login' action='auth'><span class="glyphicon glyphicon-user"> <g:message code="security.signin.label" default="Entrar"/> </span></g:link>
            </li>
        </sec:ifNotLoggedIn>
        <sec:ifLoggedIn>
            <li class="active">
                <a class="dropdown-toggle" role="button" data-toggle="dropdown" data-target="#" href="#"><span class="glyphicon glyphicon-user"> ${usuario?.username}</span></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="">
                        <g:link controller="logout">
                            <span class="glyphicon glyphicon-off"> <g:message code="security.signoff.label" default="Salir"/></span>
                        </g:link>
                    </li>
                </ul>
            </li>
        </sec:ifLoggedIn>

        <g:render template="/_menu/reloj"/>
    <%--
    <g:render template="/barraNavegacion/buscador"/>
    --%>
    </ul>
</div>