<nav id="Navbar" class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="${createLink(uri: '/')}" class="navbar-brand">
                <img src="${resource(dir: 'images', file: 'newFram/newFram.jpeg')}" />
            </a>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse" role="navigation">

            <ul class="nav navbar-nav navbar">
                <g:render template="/_menu/menu"/>

            </ul>

        </div>
    </div>
</nav>