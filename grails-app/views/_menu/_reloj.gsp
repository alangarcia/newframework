<li class="">
    <a href="#" id="reloj"><g:message code="reloj.horaActual.label" default="Hora actual"/> </a>
    <div id="horaActual"></div>
</li>


<script>
    $(document).ready(function(){
        dameTiempo();
    });

    function dameTiempo() {
        var fechaActual =   new Date();
        var horaActual  =   fechaActual.getDate() + " de " + dameMesLetra(fechaActual.getMonth()) + " de " + fechaActual.getFullYear() + " - " + dameDigitos(fechaActual.getHours()) + ":" + dameDigitos(fechaActual.getMinutes()) + ":" + dameDigitos(fechaActual.getSeconds());
        $('#reloj').text(horaActual);
        setTimeout(function(){dameTiempo()},500);
    }

    function dameMesLetra(mesNumero) {
        var mes = new Array();
        mes[0]="Enero";
        mes[1]="Febrero";
        mes[2]="Mazo";
        mes[3]="Abril";
        mes[4]="Mayo";
        mes[5]="Junio";
        mes[6]="Julio";
        mes[7]="Agosto";
        mes[8]="Septiembre";
        mes[9]="Octubre";
        mes[10]="Noviembre";
        mes[11]="Diciembre";

        return mes[mesNumero];
    }

    function dameDigitos(tiempo) {
        if (tiempo < 10) {
            tiempo = "0" + tiempo
        }
        return tiempo
    }
</script>