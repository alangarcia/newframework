<!--
This menu is used to show function that can be triggered on the content (an object or list of objects).
-->

<%-- Only show the "Pills" navigation menu if a controller exists (but not for home) --%>
<g:if test="${	params.controller != null
			&&	params.controller != ''
			&&	params.controller != 'home'
}">
    <div class="col-md-1">
        <ul id="Menu" class="nav nav-pills nav-stacked margin-top-small">

            <g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />

            <g:if test="${params.controller != 'tgsManCatProducto'}">
                <li class="${ params.action == "list" ? 'active' : '' }">
                    <g:link action="list"><i class="glyphicon glyphicon-th-list"></i></g:link>
                </li>
            </g:if>

            <g:if test="${params.controller == 'tgsManCatProducto'}">
                <li class="${ params.action == "buscar" ? 'active' : '' }">
                    <g:link action="buscar"><i class="glyphicon glyphicon-search"></i></g:link>
                </li>
            </g:if>
            <g:if test="${params.controller != 'tgsManCatProducto'}">
                <li class="${ params.action == "create" ? 'active' : '' }">
                    <g:link action="create"><i class="glyphicon glyphicon-plus"></i></g:link>
                </li>
            </g:if>
            <g:if test="${params.controller != 'tgsManSolicitud' && params.controller != 'tgsManCatProducto'}">
                <li class="${ params.action == "importar" ? 'active' : '' }">
                    <g:link action="importar"><i class="glyphicon glyphicon-upload"></i></g:link>
                </li>
            </g:if>


            <g:if test="${ params.action == 'show' || params.action == 'edit' }">
                <g:if test="${params.controller != 'tgsManSolicitud'}">
                    <li class="${ params.action == "edit" ? 'active' : '' }">
                        <g:link action="edit" id="${params.id}"><i class="glyphicon glyphicon-edit"></i></g:link>
                    </li>
                </g:if>
                <li class="">
                    <g:render template="/_common/modals/deleteTextLink"/>
                </li>
            </g:if>

        </ul>
    </div>
</g:if>
