import com.seguridad.Requestmap
import com.seguridad.Rol
import com.seguridad.Usuario
import com.seguridad.UsuarioRol

class BootStrap {

    def init = { servletContext ->
                Rol rolAdmin = new Rol(authority: 'ROLE_ADMIN').save(flush: true)
                Usuario administrador = new Usuario(username: 'administrador', password: '1234').save(flush: true)
                UsuarioRol.create(administrador, rolAdmin, true)

                //assert Usuario.count() == 1
                //assert Rol.count() == 1
                //assert UsuarioRol.count() == 1
                for (String url in [
                                '/',
                                '/index',
                                '/about',
                                '/contact',
                                '/systeminfo',
                                '/index.gsp',
                                '/**/favicon.ico',
                                '/**/js/**',
                                '/**/css/**',
                                '/**/images/**',
                                '/login',
                                '/login.*',
                                '/login/*',
                                '/logout',
                                '/logout.*',
                                '/logout/*'
                                        ]) {
                        new Requestmap(url: url, configAttribute: 'permitAll').save()
                    }
    }
    def destroy = {
 }
}